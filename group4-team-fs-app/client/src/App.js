import React, { useState } from "react";
import "./App.css"; // Import the CSS file
import MedXpressedited from "./assets/MedXpressedited.jpg"; // Import the image
import { Fade } from "react-awesome-reveal";

function PharmacyLogApp() {
  const [showErrorModal, setShowErrorModal] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    const passwordInput = event.target.password.value;
    if (
      !/\d/.test(passwordInput) ||
      !/[!@#$%^&*(),.?":{}|<>]/.test(passwordInput)
    ) {
      setShowErrorModal(true);
      return;
    }
    // perform login logic here
  };

  return (
    <>
      <header>
        <img className="logo" src={MedXpressedited} alt="MedXpress logo" />
        <h1>MedXpress</h1>
      </header>
      <main>
        <Fade direction="right" delay={1000}>
          <div className="container">
            <form onSubmit={handleSubmit}>
              <h2>Log In</h2>
              <label htmlFor="username">Username</label>
              <input type="text" id="username" name="username" required />
              <label htmlFor="password">Password</label>
              <input type="password" id="password" name="password" required />
              <button type="submit">Log In</button>
            </form>
            <div className="cta">
              <a href="#">Create an Account</a>
            </div>
          </div>
        </Fade>
      </main>
      <footer>
        <p>&copy; 2023 Pharmacy Log App. All rights reserved.</p>
      </footer>
      {showErrorModal && (
        <Fade direction="bottom">
          <div className="modal">
            <div className="modal-content">
              <h2>Error! 😓</h2>
              <p>
                Password must contain at least one number and one special
                character.
              </p>
              <button onClick={() => setShowErrorModal(false)}>Close</button>
            </div>
          </div>
        </Fade>
      )}
    </>
  );
}

export default PharmacyLogApp;
