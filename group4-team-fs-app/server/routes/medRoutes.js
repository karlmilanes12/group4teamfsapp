const express = require("express");
const router = express.Router();

// TEST Controller
const { uploadTest } = require("../controller/testController.js");

// TEST Routes
router.route("/test/uploadTest").post(uploadTest);

module.exports = router;
