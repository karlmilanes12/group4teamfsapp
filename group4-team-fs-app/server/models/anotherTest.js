const mongoose = require("mongoose");

const anotherTestSchema = new mongoose.Schema({
  name: { type: String, required: true },
  clinic: { type: String, required: true },
});

module.exports = mongoose.model("AnotherTest", anotherTestSchema);
