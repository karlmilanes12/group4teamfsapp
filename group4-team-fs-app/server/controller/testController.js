const Test = require("../models/testModel");

//POST http://localhost:3000/test/uploadTest
const uploadTest = async (req, res) => {
  try {
    const { name, clinic } = req.body;

    if (!name || !clinic) {
      res.status(400).json({ error: "Name and clinic are required" });
      return;
    }

    const newTest = new Test({
      name,
      clinic,
    });

    await newTest.save();
    res.status(201).json({ message: "Test uploaded successfully" });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

module.exports = {
  uploadTest,
};
